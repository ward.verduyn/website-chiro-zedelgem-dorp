/* Van dit deel vanboven moet je afblijven tenzij je weet wat je aan het doen bent */

/* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */

function hamburgerMenu() {
    let x = document.querySelector(".navbar");
    if (x.className === "navbar") {
        x.className = "navbar show";
    } else {
        x.className = "navbar";
    }
}

function pagina(naam, bestand) {
    let y = ''
    if (window.location.pathname.split("/").pop() === bestand || window.location.pathname.split("/").pop() === bestand.split(".").shift()) {
        y = '<li><a href="' + bestand + '" class="active-menu">' + naam + '</a></li>';
    } else {
        y = '<li><a href="' + bestand + '">' + naam + '</a></li>';
    }
    return y
}