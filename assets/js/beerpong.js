// Stel de datum en tijd van het evenement in
const eventDate = new Date("2025-03-29T16:00:00+01:00");

// Functie voor de aftelklok
function countdown() {
    // Haal de huidige datum en tijd op
    const now = new Date().getTime();

    // Vind het verschil tussen nu en de datum van het evenement
    const distance = eventDate - now;

    // Tijd berekeningen voor dagen, uren, minuten en seconden
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Resultaat weergeven in de juiste elementen
    document.getElementById("dagen").innerHTML = days;
    document.getElementById("uren").innerHTML = hours;
    document.getElementById("minuten").innerHTML = minutes;
    document.getElementById("seconden").innerHTML = seconds;

    // Als de aftelklok is afgelopen, verberg de div
    if (distance < 0) {
        clearInterval(interval);
        document.getElementById("aftelklok").style.display = "none";
    }
}

// Roep de functie onmiddellijk aan
countdown();

// Update de aftelklok elke seconde
const interval = setInterval(countdown, 1000);