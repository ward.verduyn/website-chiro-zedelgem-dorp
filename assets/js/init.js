document.addEventListener('DOMContentLoaded', init);

function init() {
    document.querySelector(".hamburger").addEventListener("click", hamburgerMenu);

    document.querySelector(".navbar").innerHTML = "<ul>"
        + pagina("Home", "/")
        + pagina("Info","info")
        + pagina ("Kriebel", "kriebel")
        + pagina ("Verhuur", "verhuur")
        + pagina("Contact", "contact")

        + "</ul>"
}